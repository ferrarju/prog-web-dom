<?php
    require_once('libcalcul.php'); // appelle d'un fichier extérieur
    $somme=$_GET[somme];
    $taux=$_GET[taux];
    $annee=$_GET[annee];
    /* utilisation via la fonction post
    $somme=$_POST['somme'];
    $taux=$_POST['taux'];
    $annee=$_POST['annee'];
    */
    
    /* utilisation en argument de la fonction
    $somme=$argv[1];
    $taux=$argv[2];
    $annee=$argv[3];
    */
    /* utilisation sans fonction cumul
    *$solde=$somme*(1+$taux/100)**$annee;
    */
    $solde=cumul($somme,$taux,$annee);
    $interet=$solde-$somme;


    echo "solde:$solde "; 
    echo "interet:$interet ";

?>