% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : l'api TMDB

1) Observation de la réponse lorsque l'on rentre l'URL : http://api.themoviedb.org/3/movie/550?api_key=ebb02613ce5a2ae58fde00f4db95a9c1 
On remarque que le format est un JSON avec différente catégorie comme title, original title ou encore overview
lorsque l'on ajoute le paramètre language= fr on remarque que certaine partie on été traduite comme overview.  	

2) l'utilisation des fonctions du fichier tp3-helper.php permet de facilement ajouter des paramètre et de modifier le lien envoyer par curl 

3) on utilise les fonction pour récupérer le fichier JSON correspondant puis on le convertit en talbeau associatif et on récupère les valeur intéressante comme "title" ou "release date"

4) On constate que l'on doit faire appel trois fois à l'api, il peut être judicieux de vérifier si l'id renseigner correspond bien à un film

5)Lors de l'affichage de l'affiche on doit formuler la taille ainsi que l'image path et on obtient alors un url pour afficher l'image nous avons choisie en taille 550. On pense d'abord à véirifer l'existence d'une affiche avant d'essayer de l'afficher

6) pour afficher une collection à partir d'un nom on doit utiliser la fonction search on à la liste des collections qui on le nom et on utilise leurs id pour en savoir plus (comme la liste des films)

7) pour cela il suffit de récupérer la liste des acteurs de chaque film et les ajouter à un tableau global qui correspond à l'ensemble des films de la collection 

8) On pourrait en partie afficher la liste des hobbit dont le mot hobbit est mentionner dans la partie "character" mais on ne pourrait pas afficher tout les Hobbits du film car par exemple Frodon est un hobbit mais ce n'est pas renseigner 

9) Pour avoir la filmographie de l'acteur il suffit simplement d'avoir l'id de l'acteur et on intérroge l'api pour avoir la liste des film ce qui est assez facile

10) pour récupérer le lien de la vidéo youtube on doit de nouveau intérroger l'api avec le paramètre "append_to_response"=>"videos" afin d'avoir la liste des vidéos.
le format est le code youtube auquel on doit rahjouter précédement "https://www.youtube.com/watch?v=" on prend systématiquement la première vidéo 
pour le embed on utilise l'outil donner par youtube où l'on doit juste renseigner l'url de la vidéo que l'on viens d'obtenir

Partie deux mise en jambe avec l'analyse flux RSS d'un podcast


1) pour obtenir le flux rss du lien, on utiliser Feed::loadRss
ensuite on récupère les valeurs en convertissant en tableau le rss
puis on parcours chaque podcast avec foreach des item 
Pour récupérer la durée on récupère la description et on prend la première partie
Pour télécharger le fichier MP3 on utilise download qui ne fonctionne que sur chrome, car firefox a une sécurité qui empêche de télécharger une ressource qui n'est pas sur le même nom de domaine

## Participants 

* Thomazo Corentin
* Julien Ferrari



