<html>
    <head>
        <title>resultat recherche</title>
        <link rel="stylesheet" type="text/css" href="basic.css?ts=<?=time()?>" />

    </head>
    <body>
        <div class="content">

            <div class="navbar">  
                <a href=index.php>accueil </a>
                <a href=film.php>film </a>
                <a href=collection.php> collection </a>
            </div>
            <?php
                
                require_once("tp3-helpers.php");
                require_once("tableau.php");
                $titre=$_GET[search];
                if(isset($_GET[search])){
                    // vérifie si le champs search est bien remplie
                    $collectionOrNot=$_GET[collection];
                    echo"Voici les résultat de recherche pour :".$titre;
                    if (isset($_GET[collection])){
                        // vérifie si la checkbox a été coché donc que l'on cherche une collection
                        $resultsearchcollectionJSON=tmdbget("search/collection",array("query"=>$titre));
                        /*cherche le lien https://api.themoviedb.org/3/search/collection?api_key={api_key}&query={titre} */
                        $resultsearchcollection=json_decode($resultsearchcollectionJSON,true);
                        $listresultcollection=$resultsearchcollection[results];
                        affichagelistcollection($listresultcollection);
                    }else{
                        // on cherche donc un film 
                        $resultsearchmovieJSON=tmdbget("search/movie",array("query"=>$titre));
                        /*cherche le lien https://api.themoviedb.org/3/search/movie?api_key={api_key}&query={titre} */

                        $resultsearchmovie=json_decode($resultsearchmovieJSON,true);
                        $listresultmovie=$resultsearchmovie[results];
                        affichagelistmovie($listresultmovie);
                    }
                }
            ?>
        </div>
    </body>
    

</html>