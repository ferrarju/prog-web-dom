<html>
    <head>
        <title> Collection</title>
        <link rel="stylesheet" type="text/css" href="basic.css?ts=<?=time()?>" />


    </head>
    <body>
        <div class="content">

            <div class="navbar">  
                <a href=index.php>accueil </a>
                <a href=film.php>film </a>
                <a href=collection.php> collection </a>
            </div>
            <p>
                 <!--formulaire qui amène vers collection.php avec l'id en get -->
                <h1>Je connais l'id  de la collection </h1>
                <form method="get" action="collection.php">
                    <label for="a">entrer l'id de la collection</label> <input type="text" id="page_collection" name="page_collection"/> <br />
                    <input type="submit" />
                </form>
            </p>
            <?php
                require_once("tp3-helpers.php");
                require_once("tableau.php");
                if(isset($_GET[page_collection])&&($_GET[page_collection]!="")){
                    //vérifie si un id a été entrée
                    $id_collection=$_GET[page_collection];
                    $content_JSON_collection=tmdbget("collection/".$id_collection);
                    $content_collection_list_movie=json_decode($content_JSON_collection,true);
                    $parts=$content_collection_list_movie[parts];
                    if(isset($parts)){
                            // vérifie si l'id choisie correspond à une collection
                        echo "<h1>liste des films </h1>";
                        collection($parts);
                        echo "<h1>liste des acteurs </h1>";
                        list_actor($parts);
                    }else{
                        echo "<h1>l'id renseigné ne correspond à aucune collection</h1>";
                    }
                }else{
                    echo "<h1> Erreur aucun id correspondant";
                }
            ?>
        </div>
        
    </body>
</html>