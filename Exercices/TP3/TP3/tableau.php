<?php
    /*
    * fonction qui compte le nombre de film d'un acteur 
    * récupère l'id de l'acteur
    * reourne un entier
    */
    function nombrefilm($id_actor){
        $content_JSON_filmographie=tmdbget("person/".$id_actor."/movie_credits",array("language"=>"en-US"));
        $content_collection_filmographie=json_decode($content_JSON_filmographie,true);
        // obtention de la filmographie de l'acteur à partir de son id
        $listpers=$content_collection_filmographie["cast"];
        $count=0;
        foreach($listpers as $value){
            $count++;
        }
        return $count; 
        
    }
    /*
    * fonction qui affiche le titre, la description, 
    * en différente langue
    * récupère 3 tableau de données en 3 langue
    * return void
    * affiche un tableau de donnée
    */


    function tableau($contentoriginal,$contentfr,$contenten){
        echo "<table>";
            echo"<th>";
                echo "<td> version original </td> <td> version anglaise </td> <td> version française </td>";
            echo"</th>";
            echo"<tr>";
                echo "<td> titre original </td>"."<td>".$contentoriginal["original_title"]."</td>"."<td>".$contenten["original_title"]."</td>"."<td>".$contentfr["original_title"]."</td>";
            echo"</tr>";
            echo"<tr>";
                echo "<td> titre </td>"."<td>".$contentoriginal["title"]."</td>"."<td>".$contenten["title"]."</td>"."<td>".$contentfr["title"]."</td>";
            echo"</tr>";
            echo"<tr>";
                echo "<td> mots clé </td>"."<td>".$contentoriginal["tagline"]."</td>"."<td>".$contenten["tagline"]."</td>"."<td>".$contentfr["tagline"]."</td>";
            echo"</tr>";
            echo"<tr>";
                echo "<td> description </td>"."<td>".$contentoriginal["overview"]."</td>"."<td>".$contenten["overview"]."</td>"."<td>".$contentfr["overview"]."</td>";
            echo"</tr>";
            echo"<tr>";
                echo "<td> lien page TMB </td>";
                echo"<td> <a href='https://www.themoviedb.org/movie/".$contentoriginal["id"]."'>https://www.themoviedb.org/movie/".$contentoriginal["id"]."</a></td>";
                echo"<td> <a href='https://www.themoviedb.org/movie/".$contentoriginal["id"]."'>https://www.themoviedb.org/movie/".$contentoriginal["id"]."</a></td>";
                echo"<td> <a href='https://www.themoviedb.org/movie/".$contentoriginal["id"]."'>https://www.themoviedb.org/movie/".$contentoriginal["id"]."</a></td>";
            echo"</tr>";
        echo "</table>";

    }

    /*
    * fonction qui affiche un poster du film 
    * récupère l'image path
    * reourne void
    */

    function image($image_path){
        $link_image="https://image.tmdb.org/t/p/w500".$image_path;
        echo "<img alt='image poster film' src=".$link_image." >";
    }
    /*
    * fonction qui affiche la liste de film d'une collection 
    * récupère une liste de film
    * reourne void
    * affiche un tableau en html
    */
    function collection($list_movie){
        echo"<table>";
        echo"<th>";
                echo " <td> date </td> <td> titre </td><td> en savoir plus </td>";
            echo"</th>";
        foreach($list_movie as $value){
            echo"<tr>";         
                echo "<td>".$value["id"]."</td>";
                echo "<td>".$value["release_date"]."</td>";
                echo "<td>".$value["original_title"]."</td>";
                echo"<td><a href='film.php?page_film=".$value["id"]."'> fiche du film </a></td>";
            echo"</tr>";
        }

        
        echo"</table>";

    }
    /*
    * fonction qui permet de compter le nombre de fois qu'un acteur a tourné dans une collection 
    * récupère une liste de film
    * reourne void
    * affiche un tableau en html
    * fait appel à la fonction affichage_list_actor
    */
    function list_actor($list_movie){
        $liste_actor;
        foreach($list_movie as $value){
            $content_JSON_credits=tmdbget("movie/".$value['id'],array("append_to_response"=>"credits"));
            $content_collection_list_cast=json_decode($content_JSON_credits,true);
            $list_cast=$content_collection_list_cast["credits"]["cast"];
            foreach($list_cast as $actor){
                if ($liste_actor[$actor['id']]==null){
                    $liste_actor[$actor['id']]=array('id'=>$actor['id'],'name'=>$actor['name'],'name'=>$actor['name'],"character"=>$actor["character"],'nbfilm'=>1);
                }else{
                    $liste_actor[$actor['id']]["nbfilm"]+=1;
                }
            }
        }
        affichage_list_actor($liste_actor);

    }

    /*
    * fonction qui affiche la liste des acteurs d'une collection 
    * récupère une liste de film
    * reourne void
    * affiche un tableau en html
    */

    function affichage_list_actor($list_cast){
        echo"<table>";
        echo"<th>";
                echo " <td> name </td> <td> role </td><td> nombre de film</td>";
            echo"</th>";
        foreach($list_cast as $value){
            echo"<tr>";   
                echo "<td>".$value["id"]."</td>";
                echo "<td>".$value["name"]."</td>";
                echo "<td>".$value["character"]."</td>";
                echo "<td>".$value["nbfilm"]."</td>";
                echo"<td><a href='page_acteur.php?id_actor=".$value["id"]."'> filmographie </a></td>";
                // lien vers la filmographie de l'acteur
            echo"</tr>";
        }

        
        echo"</table>";

    }



    function video($id){
        $content_JSON_videos=tmdbget("movie/".$id,array("append_to_response"=>"videos"));
        $content_collection_list_video=json_decode($content_JSON_videos,true);
        $list_vid=$content_collection_list_video;
        $keyyoutube=$list_vid[videos][results][0][key];
        if ($keyyoutube!=""){
            // vérifie si une vidéo existe et l'affiche
            echo "<h2> Bande annonce du film </h2>";
            echo "<a href='https://www.youtube.com/watch?v=".$keyyoutube."'> https://www.youtube.com/watch?v=".$keyyoutube."</a>";
            // création du lien vers youtube
            echo "<iframe width='560' height='315' src='https://www.youtube.com/embed/".$keyyoutube."?controls=0' title='YouTube video player' frameborder='0' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowfullscreen></iframe>";
            // lecteur vidéo youtube intégrer
        }
    }


// fonction utilisé dans search

    /*
    * fonction qui affiche la liste des film correspondant à la recherche et les affiche dans un talbeau 
    * récupère un tableau result
    * reourne void
    * affiche un tableau en html
    */


    function affichagelistmovie($listresultmovie){
        echo "<table>";
        echo "<td> id </td><td> titre </td> <td> date </td><td> en savoir plus </td>";
        foreach($listresultmovie as $value){
        // parcours le tableau pour afficher chaque résultat
            echo"<tr>";   
            echo "<td>".$value["id"]."</td>";
            echo "<td>".$value["original_title"]."</td>";
            echo "<td>".$value["release_date"]."</td>";
            echo"<td><a href='film.php?page_film=".$value["id"]."'> fiche du film </a></td>";
            echo"</tr>";
        }
        echo "</table>";
    }

    /*
    * fonction qui affiche la liste des collections correspondant à la recherche et les affiche dans un talbeau 
    * récupère un tableau result
    * reourne void
    * affiche un tableau en html
    */

    function affichagelistcollection($listresultcollection){
        echo "<table>";
        echo "<td> id </td><td> titre </td><td> en savoir plus </td>";
        foreach($listresultcollection as $value){
            // parcours le tableau pour afficher chaque résultat
            echo"<tr>";   
            echo "<td>".$value["id"]."</td>";
            echo "<td>".$value["name"]."</td>";
            echo"<td><a href='collection.php?page_collection=".$value["id"]."'> fiche de la collection </a></td>";
            echo"</tr>";
        }
        echo "</table>";
    }
?>

     
