<html>
    <head>
        <title> film</title>
        <link rel="stylesheet" type="text/css" href="basic.css?ts=<?=time()?>" />

    </head>
    <body>
        <div class="content">

            <div class="navbar">  
                <a href=index.php>accueil </a>
                <a href=film.php>film </a>
                <a href=collection.php> collection </a>
            </div>
            <p>
                 <!--formulaire qui amène vers film.php avec l'id en get -->
                <h1>Je connais l'id  du film </h1>
                <form method="get" action="film.php">
                    <label for="a">entrer l'id du film </label> <input type="text" id="page_film" name="page_film"/> <br />
                    <input type="submit" />
                </form>
            </p>
            <?php
                require_once("tp3-helpers.php");
                require_once("tableau.php");
                if(isset($_GET[page_film])&&$_GET[page_film]!=""){
                    //vérifie si un id a été entrée

                    $id_movie=$_GET[page_film];
                    $content_JSON_original=tmdbget("movie/".$id_movie);
                    $contentoriginal=json_decode($content_JSON_original,true);
                    if(isset($contentoriginal["title"])){
                        // vérifie si l'id choisie correspond à un film

                        $content_JSON_en=tmdbget("movie/".$id_movie,array("language"=>"en"));
                        $contenten=json_decode($content_JSON_en,true);
                        $content_JSON_fr=tmdbget("movie/".$id_movie,array("language"=>"fr"));
                        $contentfr=json_decode($content_JSON_fr,true);
                        echo"<h1>".$contentoriginal[title]."</h1>";
                        tableau($contentoriginal,$contentfr,$contenten);
                        if(isset($contentoriginal["poster_path"])){
                            //vérifie si il un une image
                            echo"<h2>Poster du film</h2>";
                            image($contentoriginal["poster_path"]);
                        }
                        video($id_movie);
                    }else{
                        echo "<h1>l'id renseigné ne correspond à aucun film</h1>";
                    }

                }else{
                    echo "<h1> Erreur aucun id correspondant</h1>";
                }
            ?>
        </div>
        
    </body>
    

</html>