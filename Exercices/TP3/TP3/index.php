<html>
    <head>
    <title>accueil</title>
    <link rel="stylesheet" type="text/css" href="basic.css?ts=<?=time()?>" />


    </head>
    <body>
        <div class="content">
            <!--Mise en place d'un navigation bar pour pouvoir se diriger dans les différentes fenêtres -->
            <div class="navbar">  
                <a href=index.php>accueil </a>
                <a href=film.php>film </a>
                <a href=collection.php> collection </a>
            </div>
            <p>
                <!--formulaire qui amène vers search et get un text ainsi qu'une checkbox pour savoir si c'est une collection -->
                <h1>Je connais le titre du film ou de la collection</h1>
                <form method="get" action="search.php">
                    <label for="a">entrer le nom du film ou de la collection</label> <input type="text" id="search" name="search"/> <br />
                    <p>Est-ce une collection? cochez si oui <input type="checkbox" name="collection" /></p>
                    <input type="submit" />
                </form>
                </p>

            <p>
                 <!--formulaire qui amène vers film.php avec l'id en get -->
                <h1>Je connais l'id  du film </h1>
                <form method="get" action="film.php">
                    <label for="a">entrer l'id du film </label> <input type="text" id="page_film" name="page_film"/> <br />
                    <input type="submit" />
                </form>
            </p>
            <p>
                 <!--formulaire qui amène vers collection.php avec l'id en get -->
                <h1>Je connais l'id  de la collection </h1>
                <form method="get" action="collection.php">
                    <label for="a">entrer l'id de la collection</label> <input type="text" id="page_collection" name="page_collection"/> <br />
                    <input type="submit" />
                </form>
            </p>
        </div>
    </body>
</html>