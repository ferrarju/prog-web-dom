<html>
    <head>
        <title>borne wifi</title>
    </head>
    <body>
        table
       <table>
            
            <?php
            require_once("tp2-helpers.php");
            //-------------------------------------------
            // Question 3
            /* $lines=file("borneswifi.csv");
                // on commence à -1 car la première ligne ne correspond pas à une réelle borne wifi
                $counter=-1;
                foreach ($lines as $line_num => $line) {
                    echo  $line;
                    $counter++;
                }
                echo "$counter";
            */
            //-------------------------------------------
            // question 4
            /*$row = 1;
            $tab;
            if (($handle = fopen("borneswifi.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    $tab[$row]=['name'=>$data[0],'adr'=>$data[1],'lon'=>$data[2],'lat'=>$data[3]];
                    $row++;
                }
                fclose ($handle);
            }
            print_r($tab);
            */
            //-------------------------------------------
            // question 5
            /*$row = 1;
            $counter;
            $min200;
            $tab;
            $min;
            //on utilise geopoint pour creer un talbeau avec la longitude et la lattitude
            $position=geopoint('5.727341','45.191064');
            if (($handle = fopen("borneswifi.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    // on ajoute à tab la distance qui est calculé avec la fonction distance
                    $tab[$row]=['name'=>$data[0],'adr'=>$data[1],'lon'=>$data[2],'lat'=>$data[3],'dist'=>distance($position,geopoint($data[2],$data[3]))];
                    
                    // calcul du minimum;
                    if(!isset($min)||$tab[$row]['dist']<$min['dist']){
                        $min=[$tab[$row]['name'],$tab[$row]['adr'],'dist'=>$tab[$row]['dist']];
                    }

                    // ajoute dans le tableau min 200 les bornes qui sont à moins de 200 mètre de la position
                    if($tab[$row]['dist']<200){
                        $min200[$counter]=[$tab[$row]['name'],$tab[$row]['adr'],$tab[$row]['dist']];
                        $counter++;
                    }
                    $row++;
                }
                fclose ($handle);
            }

            print_r($tab);
            echo "la borne la plus proche est: \n ";
            print_r( $min);
            echo "\n";
            echo "le nombre de borne qui ont une distance inférieur à 200 m est de:";
            echo $counter."\n";
            print_r($min200);
            */
            //-------------------------------------------
            // question 6
            /*$nbtop=5;
            $row = 1;
            $counter;
            $tabdist;
            $tab;
            // mise en place d'un paramètre top qui peut être renseigner dans une url
            $position=geopoint('5.727341','45.191064');
            if(isset($_GET[top])){
                $nbtop=$_GET[top];
            }
            if (($handle = fopen("borneswifi.csv", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    $num = count($data);
                    $tab[$row]=['name'=>$data[0],'adr'=>$data[1],'lon'=>$data[2],'lat'=>$data[3],'dist'=>distance($position,geopoint($data[2],$data[3]))];
                    $tabdist[$row]=array($tab[$row]['dist']);       
                    $row++;
                    }
                fclose ($handle);
            }

            // trie de tab 
            array_multisort($tabdist, SORT_ASC, $tab);


            // affichage des nbtop bornes les plus proche de $position 
            /*for ($i=0;$i<$nbtop;$i++){
                echo $tab[$i]['name'].", ".$tab[$i]['adr'].", ".$tab[$i]['dist']."m"."\n";
            }*/
            //-------------------------------------------
            // question 7
            /*
            // test de l'API pour obtenir l'addresse de la borne la plus proche de la position
                // création de l'url
                $url="http://api-adresse.data.gouv.fr/reverse/?lon=".$tab[0]['lon']."&lat=".$tab[0]['lat'];
                //récupération de la réponse de l'api
                $addresse=json_decode(smartcurl($url,0),true);
                // récupération des différentes données
                $output[0]=['name'=>$tab[0]['name'],'adr'=>$addresse['features'][0]['properties']['label'],'lon'=>$tab[0]['lon'],'lat'=>$tab[0]['lon'],'dist'=>$tab[0]['dist']];
                // création du tableau
                $fichierJSON=json_encode($output);
                print_r($fichierJSON);
            */
            //-------------------------------------------
            // question 8/9
            // on utilise le code de question 6 auquel on ajoute différents éléments
                $nbtop=5;
                $nblat=0;
                $nblon=0;
                if(isset($_GET[top])){
                    $nbtop=$_GET[top];
                }
                if(isset($_GET[lon])){
                    $nblon=$_GET[lon];;
                }
                if(isset($_GET[lat])){
                    $nblat=$_GET[lat];;
                }
                $row = 1;
                $counter;
                $min200;
                $tabdist;
                $tab;
                $min;
                $position=geopoint($nblon,$nblat);
                $output;
                if (($handle = fopen("borneswifi.csv", "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $num = count($data);
                        $tab[$row]=['name'=>$data[0],//'adr'=>$data[1],
                        'lon'=>$data[2],'lat'=>$data[3],'dist'=>distance($position,geopoint($data[2],$data[3]))];
                        $tabdist[$row]=array($tab[$row]['dist']);
                        if(!isset($min)||$tab[$row]['dist']<$min){
                            $min=$tab[$row]['dist'];
                        }
                    $row++;
                    }
                }
                if ($nbtop>$row){//régulation du nombre entrée en top pour éviter que le nombre soit supérieur au nombre de donné
                    $nbtop=$row-1;
                fclose ($handle);
                }
                array_multisort($tabdist, SORT_ASC, $tab);
                /*for($i=0;$i<$nbtop;$i++){
                    $url="http://api-adresse.data.gouv.fr/reverse/?lon=".$tab[$i]['lon']."&lat=".$tab[$i]['lat'];
                    $addresse=json_decode(smartcurl($url,0),true);
                    $output[$i]=[$tab[$i]['name'],'adr'=>$addresse['features'][0]['properties']['label'],$tab[$i]['lon'],$tab[$i]['lon'],$tab[$i]['dist']];
                }
                $fichierJSON=json_encode($output);
                print_r($fichierJSON);*.*/
            //-------------------------------------------
            // question 10
            // on réutilise le code de 9 on change juste la présentation
            for($i=0;$i<$nbtop;$i++){
                $url="http://api-adresse.data.gouv.fr/reverse/?lon=".$tab[$i]['lon']."&lat=".$tab[$i]['lat'];
                $addresse=json_decode(smartcurl($url,0),true);
                $output[$i]=[$tab[$i]['name'],$addresse['features'][0]['properties']['label'],$tab[$i]['lon'],$tab[$i]['lon'],$tab[$i]['dist']];

            for($i=0;$i<$nbtop ; $i++){
                echo"<tr>";
                
                for($j=0;$j<5 ; $j++){
                    echo  "<td>";
                    echo $i;
                   // echo $output[$i];
                    echo"</td>";
                }
                echo "</tr>";
                
            }
            ?>
       </table>
       

    </body>
</html>